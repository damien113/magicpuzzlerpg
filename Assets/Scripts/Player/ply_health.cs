﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ply_health : MonoBehaviour
{
    #region Core Class Variables
    /// <summary>
    /// The maximum amount of health the player can have.
    /// </summary>
    public int maxHitPoints;
    /// <summary>
    /// The amount of health the player currently has.
    /// </summary>
    public int hitPoints;
    #endregion

    private void Start()
    {
        Messenger.AddListener<int, bool>("vitality_stat_changed", SetMaxHealth);
    }

    /// <summary>
    /// This function damages the player. It triggers a event "ply damaged_event"
    /// </summary>
    /// <param name="The gameobject of what attacked the player."></param>
    /// <param name="The amount of damage to apply."></param>
    public void Damage(GameObject attacker , int damage)
    {
        int tmp = hitPoints - damage;
        if (tmp>0)
        {
            hitPoints = tmp;
        }
        else
        {
            Kill(attacker);
        }
        Messenger.Broadcast<GameObject, int>("ply_damaged_event", attacker, damage);
    }

    /// <summary>
    /// This function heals the player. It triggers a event "ply healed_event"
    /// </summary>
    /// <param name="The gameobject of what healed the player."></param>
    /// <param name="The amount of health to apply."></param>
    public void Heal(GameObject healer, int health)
    {
        int tmp = hitPoints + health;
        if (tmp < maxHitPoints)
        {
            hitPoints = tmp;
        }
        else
        {
            hitPoints = maxHitPoints;
        }
        Messenger.Broadcast<GameObject, int>("ply_healed_event", healer, health);
    }

    /// <summary>
    /// This is a functiont that kills the player. It broadcasts am essage that theplayer died called "ply_death_event"
    /// </summary>
    /// <param name="The gameobject who killed the player."></param>
    public void Kill(GameObject killer)
    {
        Destroy(this);
        Messenger.Broadcast<GameObject>("ply_death_event", killer);
    }

    /// <summary>
    /// This will set the health of the player to a value that is passed as a argument. This was added so it could be called when the
    /// vitality stat was upgraded on the players stats. A event  called "ply_maxhpset_event" is broadcasted at the end.
    /// </summary>
    /// <param name="The amountof health to set."></param>
    /// <param name="Should the current health is to be set?"></param>
    public void SetMaxHealth(int health, bool setCurrentHealth)
    {
        if (setCurrentHealth)
        {
            maxHitPoints = health;
            hitPoints = maxHitPoints;
        }
        else
        {
            maxHitPoints = health;
        }
        Messenger.Broadcast<int>("ply_maxhpset_event", health);
    }

}
