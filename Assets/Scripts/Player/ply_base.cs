﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ply_health))]
[RequireComponent(typeof(ply_controls))]
[RequireComponent(typeof(ply_stats))]
[RequireComponent(typeof(ply_equipment))]
[RequireComponent(typeof(ply_camera))]
[RequireComponent(typeof(ply_combat))]
[RequireComponent(typeof(ply_ui))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]

public class ply_base : MonoBehaviour {
    
    #region Base Classes
    ply_health health;
    ply_controls controls;
    ply_stats stats;
    ply_equipment equipment;
    #endregion

    void Start ()
    {
        // Gets the components and sets the variables for the base classes.
        #region Get Components Needed
        health = GetComponent<ply_health>();
        controls = GetComponent<ply_controls>();
        stats = GetComponent<ply_stats>();
        equipment = GetComponent<ply_equipment>();
        #endregion
    }
}
