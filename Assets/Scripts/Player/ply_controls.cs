﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ply_controls : MonoBehaviour {

    #region Core Class Variables
    public float movementSpeed;
    public float baseWarpDistance;
    NavMeshAgent agent;
    bool canSelectElement = true;
    #endregion

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void FixedUpdate () {
        //Gets two local variables for the horizontal and vertical axis for input.
        float tmp_vrt = Input.GetAxis("Vertical");
        float tmp_hrz = Input.GetAxis("Horizontal");
        float tmp_elementselectx = Input.GetAxis("ElementSelectX");
        float tmp_elementselecty = Input.GetAxis("ElementSelectY");
        // Move the navmesh agent 
        agent.Move(new Vector3(movementSpeed * tmp_hrz * Time.deltaTime, 0, movementSpeed * tmp_vrt * Time.deltaTime));
        if (Input.GetButtonDown("Dash"))
        {
            if (tmp_vrt != 0 || tmp_hrz != 0)
            {
                //Dashes the player in the direction he was moving when he pressedt he dash button
                agent.Warp(transform.position + new Vector3(movementSpeed * tmp_hrz * Time.deltaTime, 0, movementSpeed * tmp_vrt * Time.deltaTime) * baseWarpDistance);
            }
        }
        // Try to cast a spell.
        if (Input.GetButtonDown("Cast Spell"))
        {
            if (tmp_vrt != 0 || tmp_hrz != 0)
            {
                //Dashes the player in the direction he was moving when he pressedt he dash button

                Messenger.Broadcast<Vector3>("cast_spell", new Vector3(movementSpeed * tmp_hrz * Time.deltaTime, 0, movementSpeed * tmp_vrt * Time.deltaTime));
            }
        }
        // Select element for spell
        if (canSelectElement)
        {
            if (tmp_elementselectx == 1)
            {
                Messenger.Broadcast<spellElements>("element_selected", spellElements.AIR);
                canSelectElement = false;
                StartCoroutine(elementSelectionCooldown());
            }
            else if (tmp_elementselectx == -1)
            {
                Messenger.Broadcast<spellElements>("element_selected", spellElements.FIRE);
                canSelectElement = false;
                StartCoroutine(elementSelectionCooldown());
            }
            if (tmp_elementselecty == 1)
            {
                Messenger.Broadcast<spellElements>("element_selected", spellElements.WATER);
                canSelectElement = false;
                StartCoroutine(elementSelectionCooldown());
            }
            else if (tmp_elementselecty == -1)
            {
                Messenger.Broadcast<spellElements>("element_selected", spellElements.EARTH);
                canSelectElement = false;
                StartCoroutine(elementSelectionCooldown());
            }
        }
    }


    IEnumerator elementSelectionCooldown()
    {
        yield return new WaitForSecondsRealtime(0.25f);
        canSelectElement = true;
    }
}   
