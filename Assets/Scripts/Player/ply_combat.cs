﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ply_combat : MonoBehaviour {

    #region Core Class Variables
    int _currentElementSlot = 0;

    spellElements[] _elementSlots = { spellElements.NONE, spellElements.NONE, spellElements.NONE, spellElements.NONE };

    spellElements[][] combinations = {
        new spellElements[] { spellElements.AIR, spellElements.AIR, spellElements.AIR, spellElements.AIR },
        new spellElements[] { spellElements.EARTH, spellElements.EARTH, spellElements.EARTH, spellElements.EARTH },
        new spellElements[] { spellElements.FIRE, spellElements.FIRE, spellElements.FIRE, spellElements.FIRE },
        new spellElements[] { spellElements.WATER, spellElements.WATER, spellElements.WATER, spellElements.WATER },
    };
    #endregion



    private void Start()
    {
        ResetElementSlots();
        Messenger.AddListener<spellElements>("element_selected", SetElementSlot);
        Messenger.AddListener<Vector3>("cast_spell", CastSpellEvent);
    }

    private void Update()
    {
        //Debug.Log(_elementSlots[0] + "," + _elementSlots[1] + "," + _elementSlots[2] + "," + _elementSlots[3]);
    }
    /// <summary>
    /// Called when a element button is press. It goes through the 3 slots, and if they are all taken , then it resets them all.
    /// </summary>
    /// <param name="Element To Set The Button  As."></param>
    public void SetElementSlot(spellElements element)
    {
        switch(_currentElementSlot)
        {
            case 0:
                _elementSlots[0] = element;
                _currentElementSlot++;
                //debug info for ui
                Messenger.Broadcast<spellElements>("element1changed", element);
                break;
            case 1:
                _elementSlots[1] = element;
                _currentElementSlot++;
                //debug info for ui
                Messenger.Broadcast<spellElements>("element2changed", element);
                break;
            case 2:
                _elementSlots[2] = element;
                _currentElementSlot++;
                //debug info for ui
                Messenger.Broadcast<spellElements>("element3changed", element);
                break;
            case 3:
                _elementSlots[3] = element;
                _currentElementSlot++;
                //debug info for ui
                Messenger.Broadcast<spellElements>("element4changed", element);
                break;
            case 4:
                ResetElementSlots();
                break;
        }
    }

    /// <summary>
    /// This resets all the element slots to none, and then sets the current slot to 1 so the player can choose a new combination of  elements.
    /// </summary>
    public void ResetElementSlots()
    {
        _currentElementSlot = 0;    
        _elementSlots[0] = spellElements.NONE;
        _elementSlots[1] = spellElements.NONE;
        _elementSlots[2] = spellElements.NONE;
        _elementSlots[3] = spellElements.NONE;
        //debug info for ui
        Messenger.Broadcast<spellElements>("element1changed", spellElements.NONE);
        Messenger.Broadcast<spellElements>("element2changed", spellElements.NONE);
        Messenger.Broadcast<spellElements>("element3changed", spellElements.NONE);
        Messenger.Broadcast<spellElements>("element4changed", spellElements.NONE);
    }

    public void CastSpellLogic(spells spell, Vector3 direction)
    {
        switch (spell)
        {
            case spells.AIR_BLAST:
                Debug.Log("Air Blast Casted");
                break;
            case spells.EARTH_BLAST:
                Debug.Log("Earth Blast Casted");
                break;
            case spells.FIRE_BLAST:
                Debug.Log("Fire Blast Casted");
                break;
            case spells.WATER_BLAST:
                Debug.Log("Water Blast Casted");
                break;
        }
    }

    public void CastSpellEvent(Vector3 direction)
    {
        if (_elementSlots.Contains(spellElements.NONE))
        {
            Debug.Log("INVALID COMBO");
        }
        else
        {
            int index = 0;
            foreach (spellElements[] combo in combinations)
            {
                if (combo.SequenceEqual(_elementSlots))
                {
                    CastSpellLogic((spells)index, direction);
                }
                index++;
            }
        }
    }
}
