﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ply_camera : MonoBehaviour
{
    void Update()
    {
        Vector3 viewPosition = Camera.main.WorldToViewportPoint(transform.position);

        if (viewPosition.x > 0.9f)
        {
            //move camera right if near edge
            Transform tmp = Camera.main.transform;
            tmp.position += tmp.right * GetComponent<ply_controls>().movementSpeed * Time.deltaTime;
            Debug.Log("move camera right");
        }
        else if (viewPosition.x < 0.1f)
        {
            // move camera left if near edge
            Transform tmp = Camera.main.transform;
            tmp.position += -tmp.right * GetComponent<ply_controls>().movementSpeed * Time.deltaTime;
            Debug.Log("move camera lft");
        }
        if (viewPosition.y > 0.9f)
        {
            //move camera up if near edge
            Transform tmp = Camera.main.transform;
            tmp.position += tmp.up * GetComponent<ply_controls>().movementSpeed * Time.deltaTime;
            Debug.Log("move camera up");
        }
        else if (viewPosition.y < 0.1f)
        {
            // move camera down if near edge
            Transform tmp = Camera.main.transform;
            tmp.position += -tmp.up * GetComponent<ply_controls>().movementSpeed * Time.deltaTime;
            Debug.Log("move camera down");
        }
    }
}
