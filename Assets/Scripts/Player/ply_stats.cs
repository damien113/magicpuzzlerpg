﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ply_stats : MonoBehaviour
{

    #region Core Class Variables

    /// <summary>
    /// Ditcionary to hold all the values for each stat.
    /// </summary>
    Dictionary<string, int> stats = new Dictionary<string, int>();

    /// <summary>
    /// The amount of points that the player has for stats, including the ones being used, and thes ones not.
    /// </summary>
    public int totalPoints;

    /// <summary>
    /// The amount of unused points the player has to use for stats.
    /// </summary>
    public int unusedPoints;

    /// <summary>
    /// The amount of points being used on the players stats.
    /// </summary>
    public int usedPoints;

    #endregion

    private void Start()
    {
        ResetStats();
    }

    /// <summary>
    /// Lets you set the value of a stat manually. Probally most useful for debugging and balancing.
    /// </summary>
    /// <param name="The stat to set."></param>
    /// <param name="The value to set the stat as."></param>
    public bool Set(string Stat, int Value)
    {
        if (Value >= 1)
        {
            stats[Stat] = Value;
            Debug.Log("Stat : '" + Stat + "' was set to " + Value);
            Messenger.Broadcast<string, int>("stat_updated", Stat, Value);
            Messenger.Broadcast<string, int>("stat_set", Stat, Value);
            return true;
        }
        else
        {
            Debug.Log("Attempted to set '" + Stat + "' as a invalid value. (Less than 1)");
            return false;
        }

    }

    /// <summary>
    /// This function is used to add points from a stat. Unless the amount variable is passed, then it will only add one point.
    /// </summary>
    /// <param name="The stat to add to"></param>
    /// <param name="The amount to add"></param>
    /// <returns></returns>
    public bool AddPoints(string Stat, int Amount)
    {
        if (Amount >= 1)
        {
            stats[Stat] = stats[Stat] + Amount;
            Debug.Log("Added " + Amount + " points to '" + Stat + "'");
            Messenger.Broadcast<string, int>("stat_updated", Stat, Amount);
            Messenger.Broadcast<string, int>("stat_addedpoints", Stat, Amount);
            return true;
        }
        else
        {
            Debug.Log("Attempted to add a amount of points to '" + Stat + "' That was invalid. (Less than 1) If you are trying to remove use the RemovePoints function.");
            return false;
        }
    }

    /// <summary>
    /// This function is used to remove points from a stat. Unless the amount variable is passed, then it will only remove one point.
    /// </summary>
    /// <param name="The stat to remove from"></param>
    /// <param name="The amount to remove"></param>
    /// <returns></returns>
    public bool RemovePoints(string Stat, int Amount)
    {
        if (Amount <= -1)
        {
            stats[Stat] = stats[Stat] + Amount;
            Debug.Log("Added " + Amount + " points to '" + Stat + "'");
            Messenger.Broadcast<string, int>("stat_updated", Stat, Amount);
            Messenger.Broadcast<string, int>("stat_removedpoints", Stat, Amount);
            return true;
        }
        else
        {
            Debug.Log("Attempted to add a amount of points to '" + Stat + "' That was invalid. (Less than 1) If you are trying to remove use the RemovePoints function.");
            return false;
        }
    }

    /// <summary>
    /// This function is to set all the stats back to what they are in the beggining. This is also used as a way of initially setting the stats to thier defualts in the beggining.
    /// </summary>
    public void ResetStats()
    {
        //Messenger.Broadcast< string >("stats_reset", "Stats Reset");
        stats.Add("Strength", 1);
        stats.Add("Intelligence", 1);
        stats.Add("Vitality", 1);
        stats.Add("Wisdom", 1);
    }


}
